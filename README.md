# PIM Sparse mode

>Multicast is a zero-billion-dollar industry

Cloud Native Data Center Networking: Architecture, Protocols, and Tools, By Dinesh G. Dutt, Chapter 8.

This demo demonstrates a very simple multicast routing lab on top of Cumulus Linux.

## Diagram
![alt text](https://gitlab.com/nvidia-networking/systems-engineering/poc-support/pim-sm-multicast-demo/-/raw/master/diagram/pim.png)


Once the simulation is up and running, *oob-mgmt-server* will ask you to change the default password, regardless if you login on the WebUI or through SSH.

Username: `cumulus`
Password: `CumulusLinux!`  


```shell
WARNING: Your password has expired.
You must change your password now and login again!
Changing password for cumulus.
(current) UNIX password:
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
```

## Quick testing
Listen to multicast traffic on server01:
```shell
cumulus@server01:~$ iperf -s -u -B 239.22.22.22 -i 1
```
Push traffic from server04:
```shell
cumulus@server04:~$ iperf -c 239.22.22.22 -u -T 32 -t 100 -i 1
```

## Manual flow analysis
We start from scratch, `iperf` should not be running anywhere. 

We can watch IGMP traffic coming from `server01`:
```shell
cumulus@leaf01:mgmt:~# sudo tcpdump -i swp1 -n igmp
```

At this point, we can start `iperf` on the receiver:
```shell
cumulus@server01:~$ iperf -s -u -B 239.22.22.22 -i 1
```

Leaf01 should receive IGMP JOIN from server01:
```shell
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on swp1, link-type EN10MB (Ethernet), capture size 262144 bytes
11:05:34.057523 IP 192.168.10.100 > 239.22.22.22: igmp v2 report 239.22.22.22
11:05:34.433466 IP 192.168.10.100 > 239.22.22.22: igmp v2 report 239.22.22.22
```

vlan10 should be listed in the group 239.22.22.22:
```shell
cumulus@leaf01:mgmt:~$ net show igmp groups
Interface        Address         Group           Mode Timer    Srcs V Uptime
vlan10           192.168.10.1    239.22.22.22    ---- 00:04:16    1 2 00:20:27
```

We should see (*,G) entry for the group. **Iif** points to the RP, and **OIF** points to the receiver. If there are no multicast routes, maybe PIM has been forgotten in the interface configuration.
```shell
cumulus@leaf01:mgmt:~$ ip mroute show
(0.0.0.0,239.22.22.22)           Iif: swp51      Oifs: pimreg swp51 vlan10  State: resolved
```

Spine01 should receive PIM Hello:
```shell
cumulus@spine01:mgmt:~$ sudo tcpdump -i swp1 -n '(ip proto 103) or ((ip proto 2) and (ip[20] == 0x14) or igmp)'
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on swp1, link-type EN10MB (Ethernet), capture size 262144 bytes
11:15:12.892380 IP 10.10.10.100 > 224.0.0.13: PIMv2, Hello, length 56
11:15:12.893981 IP 10.10.10.1 > 224.0.0.13: PIMv2, Hello, length 56
```

That can be checked with the **IfChannels** field, while `iperf` is still running on the receiver
```shell
cumulus@spine01:mgmt:~$ net show pim interface
Interface         State          Address  PIM Nbrs           PIM DR  FHR IfChannels
pimreg               up          0.0.0.0         0            local    0          0
swp1                 up     10.10.10.100         1            local    0          1
swp3                 up     10.10.10.100         0            local    0          0
```

And also, we can verify the state of the receiver
we can check if the receiver is in JOIN:
```shell
cumulus@spine01:mgmt:~$ net show pim join
Interface        Address         Source          Group           State      Uptime   Expire Prune
swp1             10.10.10.100    *               239.22.22.22    JOIN       00:00:02 03:28  --:--
```

Before we push traffic, let's check the state on each device:

- Receiver has join the group
```shell
cumulus@leaf01:mgmt:~$ net show pim upstream
Iif             Source          Group           State       Uptime   JoinTimer RSTimer   KATimer   RefCnt
swp51           *               239.22.22.22    J           00:05:04 00:00:08  --:--:--  --:--:--       1
```

- RP has no (*,G) entry populated
```shell
cumulus@spine01:mgmt:~$ net show pim upstream
Iif             Source          Group           State       Uptime   JoinTimer RSTimer   KATimer   RefCnt
Unknown         *               239.22.22.22    NotJ        00:05:01 --:--:--  --:--:--  --:--:--       1
```
- Sender has nothing with regard to PIM (iperf is not running)
```shell
cumulus@leaf03:mgmt:~$ net show pim upstream
Iif             Source          Group           State       Uptime   JoinTimer RSTimer   KATimer   RefCnt

cumulus@leaf03:mgmt:~$ net show mroute
IP Multicast Routing Table
Flags: S - Sparse, C - Connected, P - Pruned
       R - RP-bit set, F - Register flag, T - SPT-bit set

Source          Group           Flags   Proto  Input            Output           TTL  Uptime
```

Let's push traffic:
```shell
cumulus@server04:~$ iperf -c 239.22.22.22 -u -T 32 -t 100 -i 1
```

Server01 should receive traffic:
```shell
[  3]  0.0- 1.0 sec   129 KBytes  1.06 Mbits/sec   0.063 ms    1/   91 (1.1%)
[  3]  1.0- 2.0 sec   128 KBytes  1.05 Mbits/sec   0.062 ms    0/   89 (0%)
[  3]  2.0- 3.0 sec   128 KBytes  1.05 Mbits/sec   0.085 ms    0/   89 (0%)
[  3]  3.0- 4.0 sec   128 KBytes  1.05 Mbits/sec   0.111 ms    0/   89 (0%)
```

Now, the outputs are:
- RP
```shell
cumulus@spine01:mgmt:~$ net show pim upstream
Iif             Source          Group           State       Uptime   JoinTimer RSTimer   KATimer   RefCnt
Unknown         *               239.22.22.22    NotJ        00:07:36 --:--:--  --:--:--  --:--:--       1
swp3            192.168.20.100  239.22.22.22    J           00:00:03 00:00:26  --:--:--  00:03:01       2
```

- Sender
```shell
cumulus@leaf03:mgmt:~$ net show pim upstream
Iif             Source          Group           State       Uptime   JoinTimer RSTimer   KATimer   RefCnt
vlan20          192.168.20.100  239.22.22.22    J,RegP      00:00:06 --:--:--  00:01:14  00:03:24       2
```

- Receiver
```shell
cumulus@leaf01:mgmt:~$ net show pim upstream
Iif             Source          Group           State       Uptime   JoinTimer RSTimer   KATimer   RefCnt
swp51           *               239.22.22.22    J           00:07:40 00:00:31  --:--:--  --:--:--       1
swp51           192.168.20.100  239.22.22.22    J           00:00:07 00:00:31  --:--:--  00:03:22       1
```

Now take a look at the multicast routes
- RP
```shell
cumulus@spine01:mgmt:~$ net show mroute
IP Multicast Routing Table
Flags: S - Sparse, C - Connected, P - Pruned
       R - RP-bit set, F - Register flag, T - SPT-bit set

Source          Group           Flags   Proto  Input            Output           TTL  Uptime
192.168.20.100  239.22.22.22    ST              STAR   swp3             swp1             1    00:08:35
```
- Sender
```shell
cumulus@leaf03:mgmt:~$ net show mroute
IP Multicast Routing Table
Flags: S - Sparse, C - Connected, P - Pruned
       R - RP-bit set, F - Register flag, T - SPT-bit set

Source          Group           Flags   Proto  Input            Output           TTL  Uptime
192.168.20.100  239.22.22.22    SFT             PIM    vlan20           swp51            1    00:08:38
```

- Receiver
```shell
cumulus@leaf01:mgmt:~$ net show mroute
IP Multicast Routing Table
Flags: S - Sparse, C - Connected, P - Pruned
       R - RP-bit set, F - Register flag, T - SPT-bit set

Source          Group           Flags   Proto  Input            Output           TTL  Uptime
*               239.22.22.22    SC              IGMP   swp51            pimreg           1    00:40:12
                                                IGMP                    vlan10           1
192.168.20.100  239.22.22.22    ST              STAR   swp51            vlan10           1    00:02:44
```

## Manual deployment
### Automation
Clone the repo and deploy the configuration with the playbook:
```shell
git clone https://gitlab.com/nvidia-networking/systems-engineering/poc-support/pim-sm-multicast-demo.git
cd pim-sm-multicast-demo
ansible-playbook pim-sm.yml
```

### Configuration checklist for PIM-SM:
1. Unicast routing must be working
2. Enable PIM-SM on all interfaces, on the routers between sources and receivers
3. Enable IGMP on servers facing interfaces
4. Configure RP (rendezvous point)
5. Configure RP's address on all the routers, even on the RP itself


### Useful troubleshooting commands
NCLU:
```
$ net show pim neigh
$ net show mroute
$ net show pim
```

FRR:
```shell
$ sudo cat /var/log/frr.log
Dec 28 18:38:54 spine01 pimd[32336]: recv_join: Specified RP(10.10.10.100) in join is different than our configured RP(255.255.255.255)
```



